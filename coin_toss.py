from random import randint

while True:
    try:
        choice = input("Would you like to chose heads or tails? ").lower()
        print()
        if choice == "heads" or choice == "tails":
            coin_toss = randint(1, 100)
            if coin_toss % 2 == 0 and choice == "tails":
                print("""    /  /
 /        /
/  Tails   /
/          /
 /        /
    /  /\n""")
                print("It's tails. You win!")
            elif coin_toss % 2 == 0 and choice == "heads":
                print("""    /  /
 /        /
/  Tails   /
/          /
 /        /
    /  /\n""")
                print("It's tails. You lose.")
            elif coin_toss % 2 != 0 and choice == "tails":
                print("""    /  /
 /        /
/  Heads   /
/          /
 /        /
    /  /\n""")
                print("It's heads. You lose.")
            elif coin_toss % 2 != 0 and choice == "heads":
                print("""    /  /
 /        /
/  Heads   /
/          /
 /        /
    /  /\n""")
                print("It's heads. You win!")
        else:
            raise ValueError
        break
    except ValueError:
        pass
