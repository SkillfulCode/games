# Engrave and Save

Engrave and Save is a Python program using the Turtle graphics library that allows users to control a turtle to draw and create artistic designs. Users can move the turtle forwards and backward, turn it left and right, and clear the drawing canvas.

![Engrave and Save Screenshot](screenshot.png)

## Features

- Move the turtle forward with the 'Up' arrow key.
- Move the turtle backward with the 'Down' arrow key.
- Turn the turtle left with the 'Left' arrow key.
- Turn the turtle right with the 'Right' arrow key.
- Clear the drawing canvas with the 'c' key.
- Click anywhere on the screen to exit the program.

## Getting Started

To run the Engrave and Save program, follow these steps:

1. Make sure you have Python installed on your computer.

2. Clone this repository to your local machine or download the source code as a ZIP file.

3. Open your terminal or command prompt and navigate to the directory where the program's source code is located.

4. Run the program by executing the following command:

   ```python
   python engrave_and_save.py

5. The Turtle graphics window will open, and you can start drawing and creating your designs using the arrow keys and 'c' key to clear the canvas.

## Usage

- Use the 'Up' and 'Down' arrow keys to move the turtle forwards and backward, respectively.
- Use the 'Left' and 'Right' arrow keys to turn the turtle left and right, respectively.
- Press the 'c' key to clear the drawing canvas and reset the turtle's position.
- Click anywhere on the screen to exit the program.
