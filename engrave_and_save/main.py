from turtle import Turtle, Screen

engrave = Turtle()
screen = Screen()
screen.setup(width=1.0, height=1.0)
screen.title("Engrave and Save")
engrave.speed("fastest")
engrave.hideturtle()


def move_forwards():
    engrave.forward(10)


def move_backwards():
    engrave.backward(10)


def turn_left():
    new_heading = engrave.heading() + 90
    engrave.setheading(new_heading)


def turn_right():
    new_heading = engrave.heading() - 90
    engrave.setheading(new_heading)


def clear():
    engrave.clear()
    engrave.penup()
    engrave.home()
    engrave.pendown()


screen.listen()
screen.onkey(move_forwards, "Up")
screen.onkey(move_backwards, "Down")
screen.onkey(turn_left, "Left")
screen.onkey(turn_right, "Right")
screen.onkey(clear, "c")
screen.exitonclick()
