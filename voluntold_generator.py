from random import randint
for i in range(50):
    print()
print("Welcome to the Voluntold Generator! 🫡\n")
names = str(input("Please input a list of names, seperated by a comma, to be voluntold 🤗: "))
names = names.replace(" ", "")
names = names.replace(".", ",")
names_list = names.split(",")
voluntoldee = names_list[randint(0, len(names_list) - 1)]
voluntoldee = voluntoldee[0].upper() + voluntoldee[1:]
print(f"\nCongratulations! {voluntoldee} has been voluntold! 🥳\n")
