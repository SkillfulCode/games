from random import choice

rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

print("\nWelcome to Rock, Paper, Scissors!")

# get the game count
game_count = 0
while game_count != "3" and game_count != "1" and game_count != "one" and game_count != "three":
    game_count = input("\nBest of one, or best of three? ").lower()
print()

# convert game count to an int
if game_count == "1" or game_count == "one":
    game_count = 1
elif game_count == "3" or game_count == "three":
    game_count = 3

# have a max score variable depending on game_count to end game
# have player score variables
if game_count == 1:
    max_score = 1
elif game_count == 3:
    max_score = 2
player_score = 0
computer_score = 0

while True:
    # get players choice as input
    # assign players choice to rock, paper, or scissors
    pc = None
    while pc != "rock" and pc != "paper" and pc != "scissors":
        pc = input("Would you like to choose rock, paper, or scissors? ").lower()
    if pc == "rock":
        pc = rock
    elif pc == "paper":
        pc = paper
    elif pc == "scissors":
        pc = scissors

    # assign computers choice randomly to rock, paper, or scissors
    cc = choice([rock, paper, scissors])

    # define the outcome based on the rules of the game
    outcome = None
    if pc == rock:
        if cc == rock:
            outcome = "Draw"
        elif cc == paper:
            outcome = "Lose"
        elif cc == scissors:
            outcome = "Win"

    elif pc == paper:
        if cc == rock:
            outcome = "Win"
        elif cc == paper:
            outcome = "Draw"
        elif cc == scissors:
            outcome = "Lose"

    elif pc == scissors:
        if cc == rock:
            outcome = "Lose"
        elif cc == paper:
            outcome = "Win"
        elif cc == scissors:
            outcome = "Draw"

    # print the choices
    print(f"\nYou chose:\n{pc}")
    print(f"\nThe computer chose:\n{cc}")

    # print the outcomes
    if outcome == "Win":
        player_score += 1
        if player_score < max_score:
            print("You've won this round!!!\n")
        elif player_score >= max_score:
            print("You've won the game!!!\n")
            break
    elif outcome == "Draw":
        print("This round is a draw.\n")
    elif outcome == "Lose":
        computer_score += 1
        if computer_score < max_score:
            print("You've lost this round...\n")
        elif computer_score >= max_score:
            print("You've lost the game...\n")
            break
