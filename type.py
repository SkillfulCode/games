from random import shuffle
practice = ["`", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "\\", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "'", "z", "x", "c", "v", "b", "n", "m", ",", ".", "/", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "+", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "{", "}", "|", "A", "S", "D", "F", "G", "H", "J", "K", "L", ":", "\"", "Z", "X", "C", "V", "B", "N", "M", "<", ">", "?"]
shuffle(practice)
typed = None
print("\nWelcome to the random typing generator!")
print("""To make the most of your practice session:
• Do NOT look at your keyboard.
• Elevate your wrists above your desk.
• Keep your fingers on the home row whenever possible.
• Move your finger(s) as little as possible.
• Type each key as lightly as possible.
• Reset to the home row after typing each key.
""")
for i in practice:
    while typed != i:
        typed = input(f"Type: {i}\nHere: ")
print("Congratulations. You're finished for the day!")
