# assign a word or phrase of at least seven characters
word = None
while True:
    try:
        word = input("Choose a word or phrase of at least seven characters: ")
        if len(word) >= 7:
            temp_word = word.replace(" ", "")
            if not temp_word.isalpha():
                raise ValueError
            else:
                break
    except ValueError:
        pass

# turn the word in a lower case list
word = word.lower()
word_list = []
for i in word:
    word_list.append(i)

# this list will change if the user gets the right guess in the future
updatable_word = []
for i in range(len(word)):
    updatable_word.append(" ")

# save how many underscores there should be for the word to a list value
# unless the value is a space
underscores = []
# this word is currently a string exactly as typed
index2 = 0
for i in range(len(word)):
    # add a space to underscores if there's a space
    if word_list[index2] == " ":
        index2 += 1
        underscores.append(" ")
    else:
        index2 += 1
        underscores.append("-")
# turn underscores back into a string
underscores = " ".join(underscores)

# clear the screen for future users
for i in range(100):
    print()

# the platform variations of the game
platform0 = """
_____
|
|
|
|
|______
"""

platform1 = """
_____
|   |
|
|
|
|______
"""

platform2 = """
_____
|   |
|   O
|
|
|______
"""

platform3 = """
_____
|   |
|   O
|   |
|
|______
"""

platform4 = """
_____
|   |
|   O
|   |
|  /
|______
"""

platform5 = """
_____
|   |
|   O
|   |
|  / \\
|______
"""

platform6 = """
_____
|   |
|  \O
|   |
|  / \\
|______
"""

platform7 = """
_____
|   |
|  \O/
|   |
|  / \\
|______
"""

platforms = [
    platform0,
    platform1,
    platform2,
    platform3,
    platform4,
    platform5,
    platform6,
    platform7,
]

# create variable for saving guesses
# ensure it's a single letter
# change it to lowercase for consistency
guess = None
wrong_guess = 0
guessed_letters = ""
# end game with 7 wrong guesses
while wrong_guess < 7 and updatable_word != word_list:
    try:
        if guess == None:
            # print the starting game platform
            print(platform0)
            print(f"{underscores}")
            print()
        guess = input("Guess a letter: ")
        # ensure guess is 1 character
        if len(guess) != 1 or not guess.isalpha():
                raise ValueError
        else:
            guess = guess.lower()
            saved_guess = []
            # can keep game going here

            # if guess not in word_list increase wrong count
            if guess not in word_list:
                wrong_guess += 1
                # keep a string of incorrect guesses
                guessed_letters += f" {guess},"

            # if guess is not in word_list update updatable_word to have the letters
            index = 0
            for i in word_list:
                if i == guess:
                    updatable_word[index] = i
                    index += 1
                else:
                    index += 1
            updatable_word[0:7]
            # print the same platform is guess was right
            # else print the next platform if guess was wrong
            if guess in word_list:
                # clear the screen to make it look like it's updating
                for i in range(100):
                    print()
                # print a win statement if player won
                if updatable_word == word_list:
                    print("Congratulations! You won!!!")
                print(platforms[wrong_guess])
                print_word = " ".join(updatable_word)
                print(f"{print_word}")
                print(f"{underscores}")
                print()
                print(f"incorrect guesses:{guessed_letters}")
                print()

            else:
                # clear the screen to make it look like it's updating
                for i in range(100):
                    print()
                # print a lose statement if player lost
                if wrong_guess == 7:
                    print("You're still a great person, but... you lost :(")
                print(platforms[wrong_guess])
                print_word = " ".join(updatable_word)
                print(f"{print_word}")
                print(f"{underscores}")
                print()
                print(f"incorrect guesses:{guessed_letters}")
                print()
            # stop below here, taking out break to stay in loop until game is over
            # break
    except ValueError:
        pass
